//
//  Task.swift
//  RealmTasks
//
//  Created by Abhinav Mathur on 08/02/16.
//  Copyright © 2016 Abhinav Mathur. All rights reserved.
//

import RealmSwift

class Task: Object {
    
    dynamic var name = ""
    dynamic var createdAt = NSDate()
    dynamic var notes = ""
    dynamic var isCompleted = false
    
    
// Specify properties to ignore (Realm won't persist these)
    
//  override static func ignoredProperties() -> [String] {
//    return []
//  }
}
