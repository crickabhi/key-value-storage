//
//  TaskList.swift
//  RealmTasks
//
//  Created by Abhinav Mathur on 08/02/16.
//  Copyright © 2016 Abhinav Mathur. All rights reserved.
//

import RealmSwift


class TaskList: Object {
    
    dynamic var name = ""
    dynamic var createdAt = NSDate()
    let tasks = List<Task>()
    
// Specify properties to ignore (Realm won't persist these)
    
//  override static func ignoredProperties() -> [String] {
//    return []
//  }
}
